FROM php:7.1-apache

RUN usermod -u 1000 www-data
RUN usermod -G staff www-data

RUN a2enmod rewrite

RUN docker-php-ext-install mysqli
RUN apt-get update && apt-get install -y libpng-dev
RUN apt-get install -y \
    libwebp-dev \
    libjpeg62-turbo-dev \
    libpng-dev libxpm-dev \
    libfreetype6-dev

RUN docker-php-ext-configure gd \
    --with-gd \
    --with-webp-dir \
    --with-jpeg-dir \
    --with-png-dir \
    --with-zlib-dir \
    --with-xpm-dir \
    --with-freetype-dir \
    --enable-gd-native-ttf

RUN docker-php-ext-install gd

ADD https://ru.wordpress.org/latest-ru_RU.tar.gz /tmp/
COPY ./entrypoint.sh /tmp/
# ENTRYPOINT ["bash", "/tmp/entrypoint.sh"]
# CMD ["apache2-foreground"]
